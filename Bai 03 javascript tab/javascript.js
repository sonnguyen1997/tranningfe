
var activeTabIndex = 1;
var numberOfTabs;

window.onload=function() {
  // lấy tabcontainer
  var container = document.getElementById("tabContainer");
  // thêm vào event click tab
  var tabs = container.getElementsByTagName("li");
  // lấy ra số lượng tab
  numberOfTabs = tabs.length;
  // mỗi event click
  tabs[0].onclick=displayPage1;
  tabs[1].onclick=displayPage2;
  tabs[2].onclick=displayPage3;
  // active first tab by default
  goToTabByIndex(1);
};
// Tab 1 on-click
function displayPage1(event) {
  goToTabByIndex(1);
}

// Tab 2 on-click
function displayPage2(event) {
  goToTabByIndex(2);
}

// Tab 3 on-click
function displayPage3(event) {
  goToTabByIndex(3);
} 
 
/**
 * hiển thị tab cụ thể
 */ 
function displayTab(tabIndex) {
  document.getElementById("tabHeader_" + tabIndex).setAttribute("class","tabActiveHeader");
  document.getElementById("tabpage_" + tabIndex).setAttribute("style", "display: block");
}
 
/**
 * ẩn tab cụ thể
 */ 
function hideTab(tabIndex) {
  document.getElementById("tabHeader_" + tabIndex).removeAttribute("class","tabActiveHeader");
  document.getElementById("tabpage_" + tabIndex).setAttribute("style", "display: none");
}
 
/**
 * Use by previous / next button
 */
function goToTabByDelta(deltaIndex) {
  // Get previous/next tab 
  activeTabIndex += deltaIndex;
  if (activeTabIndex > numberOfTabs) { activeTabIndex = 1; }
  if (activeTabIndex < 1) { activeTabIndex = numberOfTabs ; }
  // lặp tab
  for (var i=1; i<=numberOfTabs; i++) {
    if (i == activeTabIndex) {
      displayTab(i);
    } else {
      hideTab(i); 
    }
  }
}

/**
 * Use by tab on-click
 */
function goToTabByIndex(newActiveTabIndex) {
  activeTabIndex = newActiveTabIndex
  // Loop over every tab 
  for (var i=1; i<=numberOfTabs; i++) {
    if (i == newActiveTabIndex) {
      displayTab(i);
    } else {
      hideTab(i); 
    }
  }
}



// ----------------------------------------------------------------------------




// ---
$('.prev').click(function(){
  $('.nav-tabs .active').prev().tab('show');//get the next tab and show it
});

$('.next').click(function(){
  $('.nav-tabs .active').next().tab('show');
});


// ---


function openCity(cityName) {
	var i; 
	var x = document.getElementsByClassName("city");
	for (i = 0; i < x.length; i++) {
		x[i].style.display = "none";
	}
	document.getElementById(cityName).style.display = "block";
}

document.getElementById("pre").addEventListener("click", function (){
	var x = document.getElementsByClassName("city");

	for (var i = 0; i < x.length; i++) {

		if(x[i].style.display != "none"){
			if(i == 0){
				var id = x.length - 1;
				x[id].style.display = "block";
			} 
			else{
				var id2 = i - 1;
				x[id2].style.display = "block";
			}
			x[i].style.display = "none";
			break;
		}
	}
});

document.getElementById("next").addEventListener("click", function (){
	var x = document.getElementsByClassName("city");

	for (var i = 0; i < x.length; i++) {

		if(x[i].style.display != "none"){
			if(i == x.length - 1){
				var id = 0;
				x[id].style.display = "block";
			} 
			else{
				var id2 = i + 1;
				x[id2].style.display = "block";
			}
			x[i].style.display = "none";
			break;
		}
	}
});