// album1

$(document).ready(function(){
		
	$('#thumbnail li').click(function(){
		var thisIndex = $(this).index()
			
		if(thisIndex < $('#thumbnail li.active').index()){
			prevImage(thisIndex, $(this).parents("#thumbnail").prev("#image-slider"));
		}else if(thisIndex > $('#thumbnail li.active').index()){
			nextImage(thisIndex, $(this).parents("#thumbnail").prev("#image-slider"));
		}
			
		$('#thumbnail li.active').removeClass('active');
		$(this).addClass('active');

		});
	});

var width = $('#image-slider').width();

function nextImage(newIndex, parent){
	parent.find('li').eq(newIndex).addClass('next-img').css('left', width);
	parent.find('li.active-img').removeClass('active-img').css('left', '0');
	parent.find('li.next-img').attr('class', 'active-img');
}
function prevImage(newIndex, parent){
	parent.find('li').eq(newIndex).addClass('next-img').css('left', -width);
	parent.find('li.active-img').removeClass('active-img').css('left', '0');
	parent.find('li.next-img').attr('class', 'active-img');
}

/* Thumbails */
var ThumbailsWidth = ($('#image-slider').width() - 18.5)/6;
$('#thumbnail li').find('img').css('width', ThumbailsWidth);

// ----------album2

$(document).ready(function() {
$('#slideshow .slide:eq(0)').css('display', 'block'); // Hien slide dau tien
var dot = $('#slideshow ul li.dot');
var slide = $('#slideshow .slide');
// Slide chay tu dong
//khai báo biến set (global)
var set;
//Hàm tự động chuyển slide
function auto() {
set = setInterval(next, 100000);
}


//Ham nut next
//Khong dat currenSlide, currentDot la global scope vi .slide-active khong co dinh
//chung la khac nhau, khong phai duy nhat
// Ham next la Global
function next() {
var currentSlide = $('#slideshow .slide-active');
var nextSlide = currentSlide.next();
var currentDot = $('#slideshow .dot-active');
var nextDot = currentDot.next();
if (nextSlide.length === 0) {
nextSlide = slide.first();
}
if (nextDot.length === 0) {
nextDot = dot.first();
}
currentSlide.fadeOut(1000).removeClass('slide-active');
nextSlide.fadeIn(1000).addClass('slide-active');
currentDot.removeClass('dot-active');
nextDot.addClass('dot-active');
}
 
$('.next').click(function() {
next();
});
// Ham nut prev
$('.prev').click(function() {
var currentSlide = $('#slideshow .slide-active');
var prevSlide = currentSlide.prev();
var currentDot = $('#slideshow .dot-active');
var prevDot = currentDot.prev();
if (prevSlide.length === 0) {
prevSlide = slide.last();
}
if (prevDot.length === 0) {
prevDot = dot.last();
}
currentSlide.fadeOut(1000).removeClass('slide-active');
prevSlide.fadeIn(1000).addClass('slide-active');
currentDot.removeClass('dot-active');
prevDot.addClass('dot-active');
});
// Ham 3 nut item
// 3 nut co cach dung giong nhau, chi khac vi tri
// Ta viet 1 ham, goi lai ham tuy theo vi tri cua item:eq
function call(n) {
$('#slideshow ul li:eq(' + n + ')').click(function() {
dot.removeClass('dot-active');
$(this).addClass('dot-active');
slide.fadeOut(1000).removeClass('slide-active');
$('#slideshow .slide:eq(' + n + ')').fadeIn(1000).addClass('slide-active');
});
}
for (var i = 0; i < dot.length; i++) {
call(i);
}
});


$(document).ready(function(){
	$('.btn-next').click(function(){
		//lấy giá trị đang active
		var currentIdx = $('.bgImage.active').index();
		//console.log(currentIdx);
		// chuyen den gia tri ke tiep
		var nextImg = $('.bgImage.active').next();
		//console.log(nextImg);
		// lay gia tri cua src
		var nextSrc = nextImg.find('img').attr('src');
		// loai bo class active
		$('.bgImage.active').removeClass('active');

		nextImg.addClass('active');
		$('#main-img').attr('src',nextSrc);

		var countImg = $('.bgImage').length;
		//console.log(countImg);
		if(currentIdx == countImg - 1){
			var headImg = $('.bgImage').eq(0);
			// console.log(headImg);
			var headSrc = headImg.find('img').attr('src')
			//console.log(nextSrc);
			$('.bgImage.active').removeClass('active');
			headImg.addClass('active');
			$('#main-img').attr('src',headSrc);
		}
	});

	$('.btn-prev').click(function(){
		//lấy giá trị đang active
		var currentIdx = $('.bgImage.active').index();
		//console.log(currentIdx);
		// chuyen den gia tri ke tiep
		var nextImg = $('.bgImage.active').prev();
		//console.log(nextImg);
		// lay gia tri cua src
		var nextSrc = nextImg.find('img').attr('src');
		// loai bo class active
		$('.bgImage.active').removeClass('active');

		nextImg.addClass('active');
		$('#main-img').attr('src',nextSrc);

		var countImg = $('.bgImage').length;
		//console.log(countImg);
		if(currentIdx == 0){
			var headImg = $('.bgImage').eq(countImg-1);
			// console.log(headImg);
			var headSrc = headImg.find('img').attr('src')
			//console.log(nextSrc);
			$('.bgImage.active').removeClass('active');
			headImg.addClass('active');
			$('#main-img').attr('src',headSrc);
		}
	});	

});


$(document).ready(function(){
            $('.bgImage').on('click','img',function(){              
                var imgsrc=$(this).attr('src');     
                //console.log(imgsrc);        
                $('#main-img').attr('src',imgsrc);
            })
        });