$(".hover").mouseleave(
  function () {
    $(this).removeClass("hover");
  }
);



$(document).ready(function() {
		$("#validation").validate({
			rules: {
				name: {   // đây là trường name của input
					required: true  // không được để trống
				},
				email: {
					required: true,
					email: true // bắt lỗi định dạng mail
				},
				message: { 
					required: true,
				},
				tel : { 
					required: true, 
					number : true, //  bắt buộc là kiểu số
					maxlength: 10 // giới hạn số ký tự được nhập vào
				}
			},
			messages: {
				name: {
					required: "Tadaaaa !!! Xin vui lòng nhập tên !"
					},
				email: {
					required: "Xin vui lòng nhập email !",
					email: "Email sai định dạng, xin vui lòng kiểm tra lại !"
					},
				message: {
					required: "Vui lòng nhập thông tin !"
					},
				tel: {
					required: "Xin vui lòng nhập số điện thoại !",
					number : "Số điện thoại bắt buộc là kiểu số !",
					maxlength : "Số điện thoại không được nhập quá 10 ký tự !"
					}
			}
		});
	});