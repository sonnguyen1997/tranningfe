<html>
<head>
	<title>Add Data</title>
</head>

<body>
<?php
//including the database connection file
include_once("classes/Crud.php");
include_once("classes/Validation.php");

$crud = new Crud();
$validation = new Validation();

if(isset($_POST['Submit'])) {	
	$name = $crud->escape_string($_POST['name']);
	$birthday = $crud->escape_string($_POST['birthday']);
	$vitri = $crud->escape_string($_POST['vitri']);
	$mucluong = $crud->escape_string($_POST['mucluong']);
	$quoctich = $crud->escape_string($_POST['quoctich']);
		
	$msg = $validation->check_empty($_POST, array('name', 'birthday', 'vitri','mucluong','quoctich'));
	
	// checking empty fields
	if($msg != null) {
		echo $msg;		
		//link to the previous page
		echo "<br/><a href='javascript:self.history.back();'>Go Back</a>";
	}	
	else { 
		// if all the fields are filled (not empty) 
			
		//insert data to database	
		$result = $crud->execute("INSERT INTO staff(ten,birthday,vitri,mucluong,quoctich) VALUES('$name','$birthday','$vitri','$mucluong','$quoctich')");
		
		//display success message
		echo "<font color='green'>Data added successfully.";
		echo "<br/><a href='index.php'>View Result</a>";
	}
}
?>
</body>
</html>
