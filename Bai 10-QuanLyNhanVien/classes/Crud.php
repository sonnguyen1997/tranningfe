<?php
include_once('config.php');
class Crud extends DbConfig
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function getData($query)
	{
		$result = $this->connection->query($query);
		
		if ($result == false) {
			return false;
		}
		
		$rows = array();
		
		while ($row = $result->fetch_assoc()) {
			$rows[] = $row;
		}
		
		return $rows;
	}
	public function execute($query)
	{
		$result = $this->connection->query($query);
		
		if ($result == false) {
			echo 'Error: cannot execute the command';
			return false;
		} else {
			return true;
				}
	}
	
	public function delete($id, $table)
	{
		$query = "DELETE FROM $table WHERE manv = $id";
		
		$result = $this->connection->query($query);
		if ($result == false) {
			echo 'Error: cannot delete id ' . $id . ' from table ' . $table;
			return false;
		} else {
			return true;
		}
	}
	
	public function escape_string($value)
	{
		return $this->connection->real_escape_string($value);
	}
	public function getList(){
		$query = "SELECT manv,ten,vitri,mucluong,quoctich,last_login, ROUND(DATEDIFF(CURDATE(), birthday) / 365, 0) AS tuoi FROM staff  ";
		$result = $this->getData($query);
		return $result;
	}
	public function search(){}
	public function getDate()
	{
		//SELECT manv,ten,vitri,mucluong,quoctich, now() FROM staff
		// $query = "SELECT DATE_FORMAT(NOW(), '%Y-%m-%d %T')";
		// $result = $this->getData($query);
		// return $result;
		// if (isset($_POST['check']))
		// {
			// 	$query = "INSERT INTO staff ( last_login)
				// 			VALUES ('alvin', now());"	;
		// }
	}
	
}

// class Pagination extends DbConfig
// {

// 	function __construct()
// 	{
// 		parent::__construct();
// 	}
// 	public function list_page(){
// 		if (isset($_GET['page'])) {
// 		$page = $_GET['page'];
// 	}
// 	else{
// 		$page = 1;
// 		}
	
	
// 	$rows_per_page = 4;
// 		// xác định vi trí page
// 	$per_row = $page * $rows_per_page - $rows_per_page;
// 		// lấy tổng số staff
// 	$total_rows = mysqli_num_rows(mysqli_query($conn,'SELECT * FROM staff'));
// 		// Tổng số page
// 	$total_pages = ceil($total_rows/$rows_per_page);
// 		// đưa ra danh sách page
// 	$list_pages = '';
	
	
// 	$page_prev = $page - 1;
// 	if ($page_prev <= 0) {
// 	 	$page_prev = 1;
// 	 }
// 	$list_pages .= '<li  class="page-item"><a class="page-link" href="index.php?page='.$page_prev.'">prev</a></li>';
// 	for ($i=1; $i < $total_pages ; $i++) {
// 	$list_pages .= '<li  class="page-item"><a class="page-link" href="index.php?page_layout=user&page='.$i.'">'.$i.'</a></li>';
// 	 }
	
	
// 		$page_next = $page + 1;
// 	if ($page_next > $total_pages) {
// 	 	$list_pages .= '';
// 	 }
// 	 else{
// 	 	$list_pages .= '<li  class="page-item"><a class="page-link" href="index.php?page_layout=user&page='.$page_next.'">next</a></li>';
// 	 }
	
	
// 		$sql = "SELECT * FROM staff Order by manv DESC Limit $per_row, $rows_per_page";
// 		$query = mysqli_query($conn,$sql);
// 	}
// }