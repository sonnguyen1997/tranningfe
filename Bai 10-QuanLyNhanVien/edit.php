

<?php include_once('classes/config.php') ?>
<?php include_once('classes/Crud.php'); ?>
<?php
$crud = new Crud();

//getting id from url
$id = $crud->escape_string($_GET['manv']);

//selecting data associated with this particular id
$result = $crud->getData("SELECT * FROM staff WHERE manv=$id");

foreach ($result as $res) {
	$ten = $res['ten'];
	$pass = $res['pass'];
	$birthday = $res['birthday'];
	$vitri = $res['vitri'];
	$mucluong = $res['mucluong'];
	$quoctich = $res['quoctich'];
}
?>
<html>
<head>	
	<head>
	<meta charset="utf-8">
	<title>Quản lý nhân viên</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
</head>
</head>

<body>
	<a href="index.php">Home</a>
	<br/><br/>
	
	<form name="form1" method="post" action="editaction.php">
		<table border="0">
			<tr> 
				<td>Tên</td>
				<td><input type="text" name="ten" value="<?php echo $ten;?>"></td>
			</tr>
			<tr> 
				<td>Tuổi</td>
				<td><input type="text" name="birthday" value="<?php echo $birthday;?>"></td>
			</tr>
			<tr> 
				<td>vị trí</td>
				<td><select name="vitri" class="">
									<option value=1>sếp</option>
									<option value=2>nhân viên</option>
								</select></td>
			</tr>
			<tr> 
				<td>Mức lương</td>
				<td><input type="text" name="mucluong" value="<?php echo $mucluong;?>"></td>
			</tr>
			<tr> 
				<td>Quốc tịch</td>
				<td><input type="text" name="quoctich" value="<?php echo $quoctich;?>"></td>
			</tr>
			<tr> 
				<td>Mật khẩu</td>
				<td><input type="password" name="pass" value="<?php echo $pass;?>"></td>
			</tr>
			<tr>
				<td><input type="hidden" name="id" value=<?php echo $_GET['manv'];?>></td>
				<td><input type="submit" name="update" value="Update"></td>
			</tr>
		</table>
	</form>
</body>
</html>
