<?php
// including the database connection file
include_once("classes/Crud.php");
include_once("classes/Validation.php");

$crud = new Crud();
$validation = new Validation();

if(isset($_POST['update']))
{	
	$id = $crud->escape_string($_POST['id']);
	
	$name = $_POST['ten'];
	$pass = $crud->escape_string($_POST['pass']);
	$birthday = $crud->escape_string($_POST['birthday']);
	$vitri = $crud->escape_string($_POST['vitri']);
	$mucluong = $crud->escape_string($_POST['mucluong']);
	$quoctich = $crud->escape_string($_POST['quoctich']);
	
	$msg = $validation->check_empty($_POST, array('ten','pass' ,'birthday', 'vitri','mucluong','quoctich'));
	
	// checking empty fields
	if($msg) {
		echo $msg;		
		//link to the previous page
		echo "<br/><a href='javascript:self.history.back();'>Go Back</a>";
	} else {	
		//updating the table
		$result = $crud->execute("UPDATE staff 
			SET ten='$name',
				pass = '$pass',
				birthday='$birthday',
				vitri='$vitri',
				mucluong='$mucluong',
				quoctich='$quoctich'

				 WHERE manv=$id");
		
		//redirectig to the display page. In our case, it is index.php
		header("Location: index.php");
	}
}
?>
