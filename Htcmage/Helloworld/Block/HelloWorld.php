<?php
namespace Htcmage\Helloworld\Block;

use Magento\Framework\Event\ManagerInterface as EventManager;
use Magento\Framework\View\Element\Template;

class HelloWorld extends Template
{
    /**
     * @var EventManager
     */
    private $eventManager;

    /*
     * @param \Magento\Framework\Event\ManagerInterface as EventManager
     */
    public function __construct(
        Template\Context $context,
        EventManager $eventManager,
        array $data = []
    ) {
        $this->eventManager = $eventManager;
        parent::__construct($context, $data);
    }


    public function helloWorld()
    {
//        $eventData = 'hello worlddddddd';
//
//        $this->eventManager->dispatch('my_module_event_after', ['myEventData' => $eventData]);
//        return $eventData;
        $textDisplay = new \Magento\Framework\DataObject(array('text' => 'Hello world'));
        $this->_eventManager->dispatch('htcmage_helloworld_display_text', ['myEventData' => $textDisplay]);
        echo $textDisplay->getText();
        exit;
    }
    protected $title;



    public function setTitle($title)
    {
        return $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }
    public function plugindemo()
    {
        echo $this->setTitle('Welcome');
        echo $this->getTitle();
    }
}
