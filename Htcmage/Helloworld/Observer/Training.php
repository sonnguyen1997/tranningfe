<?php

namespace Htcmage\Helloworld\Observer;

use Magento\Framework\Event\ObserverInterface;

class Training implements ObserverInterface
{
    public function __construct()
    {
        // Observer initialization code...
        // You can use dependency injection to get any class this observer may need.
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {

        $eventData = $observer->getData('myEventData');
        echo $eventData->getText() . " - Event </br>";
        $eventData->setText('Training file in observer');

        return $this;
    }
}
